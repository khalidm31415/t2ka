// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    callbackLogin(data);
    console.log(data);
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~:(id,first-name,last-name,site-standard-profile-request,picture-url,specialties,public-profile-url)?format=json").result(onSuccess).error(onError);

}

function liAuth(){
    IN.User.authorize(callbackLogin);
}

function liLogout(){
    IN.User.logout(callbackLogout);
}

function callbackLogin(data){
    var name = data["firstName"] + " " + data["lastName"];
    var profileURL = data["siteStandardProfileRequest"]["url"];

    var logoutButtonMenu = `<div class="right item" id="logout-button-menu"><a href="${profileURL}"><i class="user circle icon"></i><span>${name}</span></a><a class="ui inverted red button" onclick="liLogout()">Logout</a></div>`;
    var logoutButtonNavbar = `<div class="right menu" id="logout-button-navbar"><div class="item"><a href="${profileURL}"><i class="user circle icon"></i><span>${name}</span></a></div><div class="item"><a class="ui red button" onclick="liLogout()">Logout</a></div></div>`;

    $("#login-buttons-menu").replaceWith(logoutButtonMenu);
    $("#login-buttons-navbar").replaceWith(logoutButtonNavbar);
}

function callbackLogout(){
    var loginButtonMenu = `<div class="right item" id="login-buttons-menu"><a class="ui inverted button" id="login-mahasiswa-menu">Mahasiswa?</a><a class="ui inverted button" id="login-perusahaan-menu" onclick="liAuth()">Perusahaan?</a></div>`;
    var loginButtonNavbar = `<div class="right menu" id="login-buttons-navbar"><div class="item" id="login-mahasiswa-navbar"><a class="ui primary button">Cari Pekerjaan</a></div><div class="item"><a class="ui primary button" id="login-perusahaan-navbar" onclick="liAuth()">Cari Karyawan</a></div></div>`;

    $("#logout-button-menu").replaceWith(loginButtonMenu);
    $("#logout-button-navbar").replaceWith(loginButtonNavbar);
}
