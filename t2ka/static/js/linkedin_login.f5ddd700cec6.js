// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
    //IN.Event.on(IN, "logout", callbackLogout);
    //IN.Event.on(IN, "auth", shareContent);
}

// Handle the successful return from the API call
function onSuccess(data) {
    //callbackLogin(data);
    callbackCompanyLogin(data);
    var json = JSON.stringify(data)
    sessionStorage.setItem("data", json);
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~:(id,first-name,last-name,site-standard-profile-request,picture-url,specialties,public-profile-url)?format=json").result(onSuccess).error(onError);
}

function liAuth(){
    IN.User.authorize(getProfileData);
}

function liLogout(){
    IN.User.logout(callbackLogout);
    sessionStorage.clear("data");
    //$(location).attr('href', "/");
}

function callbackLogin(data){
    var name = data["firstName"] + " " + data["lastName"];
    var profileURL = data["publicProfileUrl"];
    var pictureURL = data["pictureUrl"];
    var urls = "/profile/";
    var logoutButtonMenu = `<div class="right item" id="logout-button-menu"><a href="${urls}"><img class="ui avatar image" src="${pictureURL}"><span>${name}</span></a><a class="ui inverted red button" onclick="liLogout()">Logout</a></div>`;
    var logoutButtonNavbar = `<div class="right menu" id="logout-button-navbar"><div class="item"><a href="${urls}"><img class="ui avatar image" src="${pictureURL}"><span>${name}</span></a></div><div class="item"><a class="ui red button" onclick="liLogout()">Logout</a></div></div>`;
    $("#carikerja").remove();
    $("#login-buttons-menu").replaceWith(logoutButtonMenu);
    $("#login-buttons-navbar").replaceWith(logoutButtonNavbar);
}

function callbackLogout(){
    var loginButtonMenu = `<div class="right item" id="login-buttons-menu"><a class="ui inverted button" id="login-mahasiswa-menu">Mahasiswa?</a><a class="ui inverted button" id="login-perusahaan-menu" onclick="liAuth()">Perusahaan?</a></div>`;
    var loginButtonNavbar = `<div class="right menu" id="login-buttons-navbar"><div class="item" id="login-mahasiswa-navbar"><a class="ui primary button">Cari Pekerjaan</a></div><div class="item"><a class="ui primary button" id="login-perusahaan-navbar" onclick="liAuth()">Cari Karyawan</a></div></div>`;

    $("#logout-button-menu").replaceWith(loginButtonMenu);
    $("#logout-button-navbar").replaceWith(loginButtonNavbar);
}

function onCompanyDataSuccess(data) {
    var companyName = data["name"];
    var companyPictureURL = data["squareLogoUrl"];
    var companyId = data["id"]
    var companyLinkedInURL = `http://linkedin.com/company/${companyId}`

    var logoutButtonMenu = `<div class="right item" id="logout-button-menu"><a href="${companyLinkedInURL}"><img class="ui avatar image" src="${companyPictureURL}"><span>${companyName}</span></a><a class="ui inverted red button" onclick="liLogout()">Logout</a></div>`;
    var logoutButtonNavbar = `<div class="right menu" id="logout-button-navbar"><div class="item"><a href="${companyLinkedInURL}"><img class="ui avatar image" src="${companyPictureURL}"><span>${companyName}</span></a></div><div class="item"><a class="ui red button" onclick="liLogout()">Logout</a></div></div>`;
    $("#carikerja").remove();
    $("#login-buttons-menu").replaceWith(logoutButtonMenu);
    $("#login-buttons-navbar").replaceWith(logoutButtonNavbar);
}

function onCompanyListSuccess(data){
    var companyId = data["values"][0]["id"];
    var companyName = data["values"][0]["name"];

    IN.API.Raw(`/companies/${companyId}:(id,name,ticker,description,company-type,website-url,specialties,square-logo-url)?format=json`).result(onCompanyDataSuccess).error(onError);
}

function callbackCompanyLogin(data){
    IN.API.Raw("/companies?format=json&is-company-admin=true").result(onCompanyListSuccess).error(onError);
}