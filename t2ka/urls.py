"""t2ka URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import app_login.urls as app_login
import app_profile.urls as app_profile
from app_login.views import index as index_login
import app_forum.urls as app_forum
from app_login.views import post, add_commentlog
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index_login, name='index'),
    url(r'^(?P<pk>\d+)/add_comment/$', add_commentlog, name='add_commentlog'),
    url(r'^profile/', include(app_profile,namespace='profile')),
    url(r'^forum/', include(app_forum,namespace='forum')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^(?P<pk>\d+)/$', post, name='post'),
]
