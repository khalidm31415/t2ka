[![pipeline status](https://gitlab.com/khalid.muhammad/t2ka/badges/master/pipeline.svg)](https://gitlab.com/khalid.muhammad/t1ka/commits/master)
[![coverage report](https://gitlab.com/khalid.muhammad/t2ka/badges/master/coverage.svg)](https://gitlab.com/khalid.muhammad/t1ka/commits/master)

# Anggota Kelompok
1. Ajrina Melynda Yofrizal (fitur 3: forum)
2. Ismail (fitur 4: menanggapi lowongan kerja)
3. Khalid Muhammad (fitur 1: halaman utama & login)
4. Yudho Prakoso (fitur 2: profil)


---

## Link Herokuapp
https://t2ka.herokuapp.com/