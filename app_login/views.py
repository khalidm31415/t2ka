from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from app_forum.models import Post, Comment
from app_forum.forms import Comment_Form
from django.contrib import messages
# Create your views here.
response = {}

def index(request):
    html = 'login.html'
    posting = Post.objects.all()
    response['posts'] = posting
    return render(request, html, response)

def login(request):
    html = 'loggedin.html'
    posting = Post.objects.all()
    response['posts'] = posting
    return render(request, html, response)

def post(request, pk):
    post = Post.objects.get(pk=pk)
    comment = Comment.objects.filter(post=post)
    response['comment_form'] = Comment_Form
    response['post']=post
    response['comments']=comment
    return render(request, 'postlog.html', response)

def add_commentlog(request, pk):
    form = Comment_Form(request.POST or None)
    post = get_object_or_404(Post,pk=pk)
    if(request.method == 'POST' and form.is_valid()):
        if request.POST['name'] == post.name:
            messages.info(request, 'Silakan pilih nama lain')
            return HttpResponseRedirect('/'+pk+'/')
        else:
            name = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
            content = request.POST['content']
            comments = Comment(name=name,content=content, post=post)
            comments.save()
            return HttpResponseRedirect('/'+pk+'/')
    else:
        messages.info(request, 'Komentar tidak boleh kosong')
        return HttpResponseRedirect('/'+pk+'/')
