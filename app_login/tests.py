from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from .views import index
from app_forum.models import Post, Comment

# Create your tests here.
class TestLoginApp(TestCase):
    def test_index_active(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)

    def test_postlog_addcommentlog(self):
        posts = Post(name="name", judul="judul",
                          content="content")
        posts.save()
        response = Client().get("/1/")
        self.assertEqual(response.status_code, 200)
        new_response = self.client.post('/1/add_comment/', data={'name': 'nama', 'content':'kontyen'})
        self.assertEqual(Comment.objects.all().count(), 1)
        self.assertEqual(new_response.status_code, 302)
        self.assertEqual(new_response['location'], '/1/')
