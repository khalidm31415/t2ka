$(document).ready(function() {
    // untuk halaman selain homepage navbar nya langsung dimunculin
    $('header div.top.fixed.menu').addClass('hidden');

    // fix menu when passed
    $('.masthead').visibility({
        once: false,
        onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
        },
        onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
        }
    });
});