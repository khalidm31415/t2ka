from django.test import TestCase, Client
from django.http import HttpRequest
from .views import index
from .models import Post, Comment
# Create your tests here.
class TestForumApp(TestCase):
    def test_index_active(self):
        response = Client().get("/forum/")
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        posts = Post(name="name", judul="judul",
                          content="content")
        posts.save()
        response = Client().get("/forum/post/1/")
        self.assertEqual(response.status_code, 200)

    def test_addpost_addcomment(self):
        response = self.client.post('/forum/add_post/', data={'name': 'name', 'judul' : 'judul', 'content':'kontyen'})
        self.assertEqual(Post.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/forum/')
        new_response = self.client.post('/forum/post/1/add_comment/', data={'name': 'name', 'content':'kontyen'})
        self.assertEqual(Comment.objects.all().count(), 1)
        self.assertEqual(new_response.status_code, 302)
        self.assertEqual(new_response['location'], '/forum/post/1/')
