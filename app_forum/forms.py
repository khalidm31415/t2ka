from django import forms

class Post_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    judul = forms.CharField(widget=forms.TextInput(attrs=attrs), required=True)
    content = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)

class Comment_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    content = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)
