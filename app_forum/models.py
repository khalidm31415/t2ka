from django.db import models
from tinymce.models import HTMLField
class Post(models.Model):
    name = models.CharField(max_length=27)
    judul = models.CharField(max_length=180)
    content = HTMLField()#models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

class Comment(models.Model):
    name = models.CharField(max_length=27)
    content = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, related_name='posts')
