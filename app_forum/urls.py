from django.conf.urls import url
from .views import index, add_post, post, add_comment

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_post', add_post, name='add_post'),
    url(r'^post/(?P<pk>\d+)/add_comment/$', add_comment, name='add_comment'),
    url(r'^post/(?P<pk>\d+)/$', post, name='post'),
]
