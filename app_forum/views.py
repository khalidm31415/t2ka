from django.shortcuts import render, get_object_or_404
from .forms import Post_Form, Comment_Form
from .models import Post, Comment
from django.http import HttpResponseRedirect
# Create your views here.
response = {}

def index(request):
    html = 'forum.html'
    response['post_form'] = Post_Form
    posting = Post.objects.all()
    response['posts'] = posting
    return render(request, html, response)

def add_post(request):
    form = Post_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        name = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        judul = request.POST['judul']
        content = request.POST['content']
        posts = Post(name=name, judul=judul,
                          content=content)
        posts.save()
        return HttpResponseRedirect('/forum/')

def post(request, pk):
    post = Post.objects.get(pk=pk)
    comment = Comment.objects.filter(post=post)
    response['comment_form'] = Comment_Form
    response['post']=post
    response['comments']=comment
    return render(request, 'post.html', response)

def add_comment(request, pk):
    form = Comment_Form(request.POST or None)
    post = get_object_or_404(Post,pk=pk)
    if(request.method == 'POST' and form.is_valid()):
        name = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        content = request.POST['content']
        comments = Comment(name=name,
                          content=content, post=post)
        comments.save()
        return HttpResponseRedirect('/forum/post/'+pk+'/')
