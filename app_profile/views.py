from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.
response = {}

def index(request):
    html = 'profile.html'
    return render(request, html, response)
