from django.test import TestCase, Client
from django.http import HttpRequest
from .views import index
# Create your tests here.
class TestProfileApp(TestCase):
    def test_index_active(self):
        response = Client().get("/profile/")
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn('<h3 class="ui header">Nonprofit</h3>', html_response)
