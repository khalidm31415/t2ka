from .views import index
from django.conf.urls import url, include

urlpatterns = [
    url(r'^$', index, name='index')
]
